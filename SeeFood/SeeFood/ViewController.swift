//
//  ViewController.swift
//  SeeFood
//
//  Created by Nguyen Duc Tai on 4/28/18.
//  Copyright © 2018 Nguyen Duc Tai. All rights reserved.
//

import UIKit
import VisualRecognitionV3
import SVProgressHUD
import Social
class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    let apiKey = "8f58d30f90e47a5062957c60b6454ff4a0fad1d3"
    let version = "2018-04-29"
    var arrayResult : [String] = []
    @IBOutlet weak var imageView: UIImageView!
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        SVProgressHUD.show()
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        dismiss(animated: true, completion: nil)
        let visualRecognition = VisualRecognition(apiKey: apiKey, version: version)
        let imageData = UIImageJPEGRepresentation(image, 0.01)
        let documentsURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
        let fileURL = documentsURL.appendingPathComponent("tempImage.jpg")
        try? imageData?.write(to: fileURL)
        visualRecognition.classify(imagesFile: fileURL) { (classifyImages) in
            self.arrayResult = []
            let classes = classifyImages.images.first!.classifiers.first!.classes
            for index in 0..<classes.count {
                self.arrayResult.append(classes[index].className)
            }
            print(self.arrayResult)
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            if self.arrayResult.contains("hotdog") {
                DispatchQueue.main.async {
                    self.navigationItem.title = "Hot Dog"
                }
            }
            else {
                DispatchQueue.main.async {
                    self.navigationItem.title = "Not Hot Dog"
                }
            }
        }
        
    }
    @IBAction func cameraPressed(_ sender: Any) {
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func sharePressed(_ sender: Any) {
        
    }
    

}

